<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alien
 *
 * @author pabhoz
 */
class Alien {
    //put your code here
    protected $nombre;
    private $edad;
    private $especie;
    private $planeta;
    public $moral;
    const COMUNICACION = 'Telepaticamente';

    public function __construct(string $nombre, int $edad, string $especie, string $planeta, string $moral = 'neutro')
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->especie = $especie;
        $this->planeta = new Planet($planeta);
        $this->moral = $moral;
    }

    /**
     * @param string $moral
     */
    public function setMoral($moral)
    {
        $this->moral = $moral;
    }

    public function interact() {
        echo self::COMUNICACION," dice: Hola terricola mi nombre es {$this->nombre}, vinimos en son de paz";
    }

    public function whoIAm() {
        echo self::COMUNICACION," dice: Mi nombre es {$this->nombre}, vengo del plantea {$this->planeta->getPlaneta()}, soy un {$this->especie} y soy {$this->moral}";
    }
}
