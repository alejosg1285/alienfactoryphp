<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Planet
     *
     * @author pabhoz
     */
    class Planet
    {
        //put your code here
        private $planeta;
        private $estado;

        /**
         * Planet constructor.
         *
         * @param $planeta
         */
        public function __construct($planeta)
        {
            $this->planeta = $planeta;
            $this->estado  = 'a salvo';
        }

        /**
         * @return mixed
         */
        public function getPlaneta()
        {
            return $this->planeta;
        }

        /**
         * @param mixed $planeta
         */
        public function setPlaneta($planeta)
        {
            $this->planeta = $planeta;
        }

        /**
         * @param string $estado
         */
        public function setEstado($estado)
        {
            $this->estado = $estado;
        }

        public function status(): string
        {
            return "El planeta {$this->planeta} esta {$this->estado}";
        }
    }
