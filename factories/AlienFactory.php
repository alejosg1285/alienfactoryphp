<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of AlienFactory
     *
     * @author pabhoz
     */
    class AlienFactory
    {
        //put your code here
        public static function getAlien(string $nombre, int $edad, string $especie, string $planet)
        {
            return new Alien($nombre, $edad, $especie, $planet);
        }

        public static function getJupiterAlien(string $nombre, int $edad, string $especie)
        {
            return new JupiterAlien($nombre, $edad, $especie);
        }

        public static function getMarsAlien(string $nombre, int $edad, string $especie)
        {
            return new MarsAlien($nombre, $edad, $especie);
        }

        public static function getMoonAlien(string $nombre, int $edad, string $especie)
        {
            return new MoonAlien($nombre, $edad, $especie);
        }

        public static function getPlutoAlien(string $nombre, int $edad, string $especie)
        {
            return new PlutoAlien($nombre, $edad, $especie);
        }

        public static function getSaturnAlien(string $nombre, int $edad, string $especie)
        {
            return new SaturnAlien($nombre, $edad, $especie);
        }

        public static function getVenusAlien(string $nombre, int $edad, string $especie)
        {
            return new VenusAlien($nombre, $edad, $especie);
        }
    }
